# Making Driverless Cars Safer



## Description
Our project aims to develop a testing system to test a popular/open-source automatic driving system. 

You can find detailed information at: 

[Team04]: http://cslinux.nottingham.edu.cn/~Team202201	"http://cslinux.nottingham.edu.cn/~Team202201"



## Contributors

Zepei LUO (Leader) : scyzl9@nottingham.edu.cn

Changyang LAO: scycl7@nottingham.edu.cn

Junbo YANG: ssyjy2@nottingham.edu.cn

Peijian ZHOU: scypz1@nottingham.edu.cn

Puttipatt INGKASIT: scypi1@nottingham.edu.cn

Ziqi ZHENG: ssyzz11@nottingham.edu.cn

