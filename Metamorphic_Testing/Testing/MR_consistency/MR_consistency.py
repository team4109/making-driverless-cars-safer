#!/usr/bin/env python3

###############################################################################
# Copyright 2018 The Apollo Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import math
import time
import sys
from matplotlib import animation
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np

from distance_approach_python_interface import *

result_file = "/tmp/open_space_osqp_ipopt.csv"

# def SmoothTrajectory(visualize_flag):
def SmoothTrajectory(sx, sy, ex, ey, sphi, test):
    # initialze object
    OpenSpacePlanner = DistancePlanner()

    # parameter(except max, min and car size is defined in proto)
    num_output_buffer = 10000


    left_boundary_x = [-13.64, 0.0, 0.0]
    left_boundary_y = [0.0, 0.0, -2.82]
    down_boundary_x = [0.0, 9.15]
    down_boundary_y = [-2.82, -2.82]
    right_boundary_x = [9.15, 9.15, 16.35]
    right_boundary_y = [-2.82, 0.0, 0.0]
    up_boundary_x = [16.35, -13.64]
    up_boundary_y = [5.60, 5.60]

    bound_x = left_boundary_x + down_boundary_x + right_boundary_x + up_boundary_x
    bound_y = left_boundary_y + down_boundary_y + right_boundary_y + up_boundary_y
    bound_vec = []
    for i in range(0, len(bound_x)):
        bound_vec.append(bound_x[i])
        bound_vec.append(bound_y[i])
        ##########################


    # obstacles for distance approach(vertices coords in clock wise order)
    ROI_distance_approach_parking_boundary = (
        c_double * 20)(*bound_vec)
    OpenSpacePlanner.AddObstacle(
        ROI_distance_approach_parking_boundary)
    # parking lot position
    
    ephi = 0
    XYbounds = [min(bound_x), max(bound_x), min(bound_y), max(bound_y)]
    XYbounds_ctype = (c_double * 4)(*XYbounds)

###################################################
    success = True
    start = time.time()
    if not OpenSpacePlanner.DistancePlan(sx, sy, sphi, ex, ey, ephi, XYbounds_ctype):
        print("planning fail")
        success = False
    planning_time = time.time() - start

    return success

    



if __name__ == '__main__':
    sx = 0.0
    sy = 3
    ex = 3.29
    ey = -1.359
    sphi = 0.0
    print("Begin the source test: ")

    test = 0
    success_s = SmoothTrajectory(sx, sy, ex, ey, sphi, test)

    if success_s:
        print("Source output: \n\tIn this position, planning is successful.")
    else:
        print("Source output: \n\tIn this position, planning is not successful.")
        
    print("Begin the follow-up test: ")
    test = 1
    for i in range(1,100):
        success_f = SmoothTrajectory(sx, sy, ex, ey, sphi, test)
        if not (success_f and success_s):
            print("Follow-up output: \n\tIn" + i + "th round, planning dismatches to the source test.")
            break
    if not (success_f and success_s):
        print("Metamorphic test fail")
    else:
        print("Follow-up output: \n\tDuring 100 rounds, planning always matches to the source test.")
        print("Metamorphic test pass")
        

    sys.exit()
