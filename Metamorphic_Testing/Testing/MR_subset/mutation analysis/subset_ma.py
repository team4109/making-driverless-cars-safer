#!/usr/bin/env python3

###############################################################################
# Copyright 2018 The Apollo Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import math
import time
import sys
import numpy as np

from distance_approach_python_interface import *

def SmoothTrajectory(ex_init, parking_length):
    # initialze object
    OpenSpacePlanner = DistancePlanner()

    # parameter(except max, min and car size is defined in proto)
    num_output_buffer = 10000

    left_boundary_x = [-13.64, 0.0, 0.0]
    left_boundary_y = [0.0, 0.0, -2.82]
    down_boundary_x = [parking_length[0], parking_length[1]]
    down_boundary_y = [-2.82, -2.82]
    right_boundary_x = [parking_length[1], parking_length[1], 16.35]
    right_boundary_y = [-2.82, 0.0, 0.0]
    up_boundary_x = [16.35, -13.64]
    up_boundary_y = [5.60, 5.60]

    bound_x = left_boundary_x + down_boundary_x + right_boundary_x + up_boundary_x
    bound_y = left_boundary_y + down_boundary_y + right_boundary_y + up_boundary_y
    bound_vec = []
    for i in range(0, len(bound_x)):
        bound_vec.append(bound_x[i])
        bound_vec.append(bound_y[i])

    # obstacles for distance approach(vertices coords in clock wise order)
    ROI_distance_approach_parking_boundary = (
        c_double * 20)(*bound_vec)
    OpenSpacePlanner.AddObstacle(
        ROI_distance_approach_parking_boundary)

    # vehicle starting position coordinates
    sx = 0.0
    sy = 3
    sphi = 0.0

    # vehicle parking location coordinates
    ex = ex_init
    ey = -1.359
    ephi = 0
    # path boundary restrictions
    XYbounds = [min(bound_x), max(bound_x), min(bound_y), max(bound_y)]
    XYbounds_ctype = (c_double * 4)(*XYbounds)

    success = True
    start = time.time()
    if not OpenSpacePlanner.DistancePlan(sx, sy, sphi, ex, ey, ephi, XYbounds_ctype):
        success = False

    if success:
        return True
    else:
        return False

def subsetTest():
    # set an array of parking point x-coordinates
    ex_init = [0.50, 1.00, 1.50, 2.00, 2.50, 3.00, 3.50, 4.00, 4.50, 5.00, 5.50, 6.00]

    # set the source input: longer width parking space with start x coordinate and ending x coordinate
    parking_length_source = [0.0, 9.15]
 
    # initialize the source output: the set of successfully planned parking points
    set_source = set()

    for i in range(len(ex_init)):
        bool = SmoothTrajectory(ex_init[i], parking_length_source)
        if bool:
            set_source.add(ex_init[i])

    # set the follow-up input: shorter width parking space with start x coordinate and ending x coordinate
    parking_length_follow = [0.0, 7.48]

    # initialize the follow-up output: the set of successfully planned parking points
    set_follow = set()

    for i in range(len(ex_init)):
        bool = SmoothTrajectory(ex_init[i], parking_length_follow)
        if bool:
            set_follow.add(ex_init[i])

    test_outcome = set_follow.issubset(set_source)
    # if len(set_source) == 0:
    #     return False
    # else:
    return test_outcome
