import math
import time
import sys
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np

from distance_approach_python_interface import *

result_file = "/tmp/open_space_osqp_ipopt.csv"

def SmoothTrajectory(left_obstacle_length):
    # initialze object
    OpenSpacePlanner = DistancePlanner()

    # parameter(except max, min and car size is defined in proto)
    num_output_buffer = 10000

    left_boundary_x = [left_obstacle_length, 0.0, 0.0]
    left_boundary_y = [0.0, 0.0, -2.82]
    down_boundary_x = [0.0, 7.48]
    down_boundary_y = [-2.82, -2.82]
    right_boundary_x = [7.48, 7.48, 16.35]
    right_boundary_y = [-2.82, 0.0, 0.0]
    up_boundary_x = [16.35, left_obstacle_length]
    up_boundary_y = [5.60, 5.60]

    bound_x = left_boundary_x + down_boundary_x + right_boundary_x + up_boundary_x
    bound_y = left_boundary_y + down_boundary_y + right_boundary_y + up_boundary_y
    bound_vec = []
    for i in range(0, len(bound_x)):
        bound_vec.append(bound_x[i])
        bound_vec.append(bound_y[i])

    # obstacles for distance approach(vertices coords in clock wise order)
    ROI_distance_approach_parking_boundary = (
        c_double * 20)(*bound_vec)
    OpenSpacePlanner.AddObstacle(
        ROI_distance_approach_parking_boundary)

    # vehicle starting position coordinates
    sx = 0
    sy = 3
    sphi = 0.0 

    # vehicle parking location coordinates
    ex = 3.29
    ey = -1.359
    ephi = 0

    # path boundary restrictions
    XYbounds = [min(bound_x), max(bound_x), min(bound_y), max(bound_y)]

    # initialize the variable for DistanceGetResult() function
    x = (c_double * num_output_buffer)()
    y = (c_double * num_output_buffer)()
    phi = (c_double * num_output_buffer)()
    v = (c_double * num_output_buffer)()
    a = (c_double * num_output_buffer)()
    steer = (c_double * num_output_buffer)()
    opt_x = (c_double * num_output_buffer)()
    opt_y = (c_double * num_output_buffer)()
    opt_phi = (c_double * num_output_buffer)()
    opt_v = (c_double * num_output_buffer)()
    opt_a = (c_double * num_output_buffer)()
    opt_steer = (c_double * num_output_buffer)()
    opt_time = (c_double * num_output_buffer)()
    opt_dual_l = (c_double * num_output_buffer)()
    opt_dual_n = (c_double * num_output_buffer)()
    size = (c_ushort * 1)()
    XYbounds_ctype = (c_double * 4)(*XYbounds)
    hybrid_time = (c_double * 1)(0.0)
    dual_time = (c_double * 1)(0.0)
    ipopt_time = (c_double * 1)(0.0)

    # initialize success
    success = True

    start = time.time()
    print("\nTest with the left obstacle of the road: " + str(left_obstacle_length))

    # check if it can be planned
    if not OpenSpacePlanner.DistancePlan(sx, sy, sphi, ex, ey, ephi, XYbounds_ctype):
        print("Planning fail")
        success = False
    else:
        print("Planning success")

    planning_time = time.time() - start
    print("Planning time is " + str(planning_time))

    # initialize variable for getting parking time
    v_out = []
    opt_v_out = []
    opt_dual_l_out = []
    opt_dual_n_out = []
    parkingTime = 0

    if success:
        OpenSpacePlanner.DistanceGetResult(x, y, phi, v, a, steer, opt_x,
                                            opt_y, opt_phi, opt_v, opt_a, opt_steer, opt_time,
                                            opt_dual_l, opt_dual_n, size,
                                            hybrid_time, dual_time, ipopt_time)
        for i in range(0, size[0]):
            v_out.append(float(v[i]))
            opt_v_out.append(float(opt_v[i]))

        for i in range(0, size[0] * 6):
            opt_dual_l_out.append(float(opt_dual_l[i]))
        for i in range(0, size[0] * 16):
            opt_dual_n_out.append(float(opt_dual_n[i]))

        parkingTime = len(opt_v_out)

        print("Parking time: " + str(parkingTime))

    return [success, parkingTime]

# check if the time set is degression
def ifDegression(parkingTime):
    for i in range(len(parkingTime) - 1):
        if parkingTime[i] < parkingTime[i + 1]:
            return False
    
    return True

if __name__ == '__main__':
    print("Begin the source test: ")
    # set the source input: set the left obstacle length with -15 and -20
    left_obstacle_length_source = [-15, -20]
    print("Source input: ")
    print("\ttests with the left obstacle length of the road: " + str(left_obstacle_length_source[0]) + " and " + str(left_obstacle_length_source[1]) + ": " + str(left_obstacle_length_source))

    test_count_source = 0
    success_count_source = 0
    # initialize the source output: the set of successfully planned parking time
    actual_time_source = []

    for j in range(len(left_obstacle_length_source)):
        test_count_source += 1

        result = SmoothTrajectory(left_obstacle_length_source[j])
        actual_time_source.append(result[1])

        if result[0]:
            success_count_source += 1

    success_rate_source = success_count_source/test_count_source

    print("\nSource output: \n\tSuccess rate: " + str(success_rate_source) + "\n\tParking time set: " + str(actual_time_source) + " with left obstacle length " + str(left_obstacle_length_source[0]) + " and " + str(left_obstacle_length_source[1]))

    print("\nBegin the follow-up test: ")
    # set the follow-up input: set the left obstacle lengths of the road with the number between -15 and -20
    left_obstacle_length_follow_up = np.arange(left_obstacle_length_source[0], left_obstacle_length_source[1] - 1, -1.0)
    print("Follow-up input: ")
    print("\ttests with the left obstacle length of the road between " + str(left_obstacle_length_source[0]) + " and " + str(left_obstacle_length_source[1]) + ": " + str(left_obstacle_length_follow_up))

    test_count_follow_up = 0
    success_count_follow_up = 0
    # initialize the follow-up output: the set of successfully planned parking time
    actual_time_follow_up = []

    for i in range(len(left_obstacle_length_follow_up)):
        test_count_follow_up += 1

        result = SmoothTrajectory(left_obstacle_length_follow_up[i])
        actual_time_follow_up.append(result[1])

        if result[0]:
            success_count_follow_up += 1

    success_rate_follow_up = success_count_follow_up/test_count_follow_up

    print("\nFollow-up output: \n\tSuccess rate: " + str(success_rate_follow_up) + "\n\tParking time set: " + str(actual_time_follow_up) + " with left obstacle length between " + str(left_obstacle_length_source[0]) + " and " + str(left_obstacle_length_source[1]))

    print("\n" + str(left_obstacle_length_follow_up) + " are the detailed numbers between " + str(left_obstacle_length_source) + ", satisfies the input relation")
    
    if success_rate_source == success_rate_follow_up:
        if ifDegression(actual_time_source) == ifDegression(actual_time_follow_up) == True:
            print("\nThe success rates both are " + str(success_count_follow_up) + "\n\nThe source parking time set: " + str(actual_time_source) + "    The follow-up time set: " + str(actual_time_follow_up) + "\nThe set of follow-up parking time is inversely related to the left obstacle length like the set of source parking time, the output relation is satisfied\n")
            print("Metamorphic test pass")

    print("\nThe source success rate: " + str(success_rate_source) + "   The follow-up success rate: " + str(success_rate_follow_up) + "\nThe success rate are not equal.\n\nThe source parking time set: " + str(actual_time_source) + "   The follow-up time set: " + str(actual_time_follow_up) + "\nThe set of follow-up parking time is not inversely related to the left obstacle length like the set of source parking time, the output relation is not satisfied\n")
    print("Metamorphic test fail")

    sys.exit()
