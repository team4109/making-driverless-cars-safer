#!/usr/bin/env python3

###############################################################################
# Copyright 2018 The Apollo Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import math
import time
import sys
import matplotlib.pyplot as plt
import numpy as np

from distance_approach_python_interface import *

def SmoothTrajectory(ex_init, left_boundary):
    # initialze object
    OpenSpacePlanner = DistancePlanner()

    # parameter(except max, min and car size is defined in proto)
    num_output_buffer = 10000

    if left_boundary != 0:
        left_boundary_x = [left_boundary, 0.0, 0.0]
        left_boundary_y = [0.0, 0.0, -2.82]
        down_boundary_x = [0.0, 9.15]
        down_boundary_y = [-2.82, -2.82]
        right_boundary_x = [9.15, 9.15, 16.35]
        right_boundary_y = [-2.82, 0.0, 0.0]
        up_boundary_x = [16.35, left_boundary]
        up_boundary_y = [5.60, 5.60]
    
    if left_boundary == 0:
        left_boundary_x = [0.0, 0.0, 0.0]
        left_boundary_y = [5.60, 0.0, -2.82]
        down_boundary_x = [0.0, 9.15]
        down_boundary_y = [-2.82, -2.82]
        right_boundary_x = [9.15, 9.15, 16.35]
        right_boundary_y = [-2.82, 0.0, 0.0]
        up_boundary_x = [16.35, 0.0]
        up_boundary_y = [5.60, 5.60]

    bound_x = left_boundary_x + down_boundary_x + right_boundary_x + up_boundary_x
    bound_y = left_boundary_y + down_boundary_y + right_boundary_y + up_boundary_y
    bound_vec = []
    for i in range(0, len(bound_x)):
        bound_vec.append(bound_x[i])
        bound_vec.append(bound_y[i])

    # obstacles for distance approach(vertices coords in clock wise order)
    ROI_distance_approach_parking_boundary = (
        c_double * 20)(*bound_vec)
    OpenSpacePlanner.AddObstacle(
        ROI_distance_approach_parking_boundary)

    # vehicle starting position coordinates
    sx = 2.5
    sy = 3
    sphi = 0.0

    # parking lot position
    ex = ex_init
    ey = -1.359
    ephi = 0
    # path boundary restrictions
    XYbounds = [min(bound_x), max(bound_x), min(bound_y), max(bound_y)]
    XYbounds_ctype = (c_double * 4)(*XYbounds)

    success = True
    start = time.time()
    if not OpenSpacePlanner.DistancePlan(sx, sy, sphi, ex, ey, ephi, XYbounds_ctype):
        success = False

    planning_time = time.time() - start
    planning_time = "{:.6f}".format(planning_time)

    if ex_init == 0.5:
        print("The environment: ")

        # trajectories plot
        fig = plt.figure(1, figsize = [9,6])
        ax = fig.add_subplot(111)

        ax.plot(left_boundary_x, left_boundary_y, "k")
        ax.plot(down_boundary_x, down_boundary_y, "k")
        ax.plot(right_boundary_x, right_boundary_y, "k")
        ax.plot(up_boundary_x, up_boundary_y, "k")

        plt.axis('equal')
        plt.show()

    outcome = ", planning success"
    if not success:
        outcome = ", planning failed"
    print("\tWhen parking point x-coordinate is " + str(ex_init) + ", the planning time is " + str(planning_time) + outcome)

    if success:
        return True
    else:
        return False

if __name__ == '__main__':
    # Set an array of parking point x-coordinates
    ex_init = [0.50, 1.00, 1.50, 2.00, 2.50, 3.00, 3.50, 4.00, 4.50, 5.00, 5.50, 6.00]

    print("Begin the source test: ")

    # Set the source input: Normal environment without a left obstacle
    left_boundary = -13.64
    print("Source input: ")
    print("\tnormal environment without a left obstacle: " + str(left_boundary) + "\n")

    # Initialize the source output: The set of successfully planned parking points
    set_source = set()

    for i in range(len(ex_init)):
        bool = SmoothTrajectory(ex_init[i], left_boundary)
        if bool:
            set_source.add(ex_init[i])

    print("Source output: \n\tthe set of successfully planned parking points are")
    print("\t" + str(set_source))


    print()
    print("Begin the follow-up test: ")
    # Set the follow-up input: Another environment with a left obstacle
    left_boundary_follow = 0
    print("Follow-up input: ")
    print("\tenvironment with a left obstacle: " + str(left_boundary_follow) + "\n")

    # Initialize the follow-up output: The set of successfully planned parking points
    set_follow = set()

    for i in range(len(ex_init)):
        bool = SmoothTrajectory(ex_init[i], left_boundary_follow)
        if bool:
            set_follow.add(ex_init[i])

    print("Follow-up output: \n\tthe set of successfully planned parking points are")
    print("\t" + str(set_follow) + "\n")

    test_outcome = set_follow == set_source
    print("|" + str(left_boundary) + "|" + " > " + str(left_boundary_follow) + " = 0" + ", satisfies the input relation")
    if test_outcome:
        print(str(set_follow) + " is equal to the " + str(set_source) + "the output relation is satisfied\n")
        print("Metamorphic test pass")
    else:
        print(str(set_follow) + " is not equal to the " + str(set_source) + ". The output relation is not satisfied\n")
        print("Metamorphic test fail")