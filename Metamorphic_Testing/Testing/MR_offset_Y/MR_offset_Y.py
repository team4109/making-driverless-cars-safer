import math
import time
import sys
from matplotlib import animation
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np

from distance_approach_python_interface import *

result_file = "/tmp/open_space_osqp_ipopt.csv"

# def SmoothTrajectory(visualize_flag):
def SmoothTrajectory(visualize_flag, sx, sy, sphi, end_x, offset):
    # initialze object
    OpenSpacePlanner = DistancePlanner()

    # parameter(except max, min and car size is defined in proto)
    num_output_buffer = 10000
    #sphi = 0.0

    scenario = "parallel"
    
    if scenario == "parallel":


        #additive
        a=offset
        b=-2.82+offset
        c=5.60+offset
    
        left_boundary_x = [-13.64, 0.0, 0.0]
        left_boundary_y = [a, a, b]
        down_boundary_x = [0.0, 9.15]
        down_boundary_y = [b, b]
        right_boundary_x = [9.15, 9.15, 16.35]
        right_boundary_y = [b, a, a]
        up_boundary_x = [16.35, -13.64]
        up_boundary_y = [c, c]

    bound_x = left_boundary_x + down_boundary_x + right_boundary_x + up_boundary_x
    bound_y = left_boundary_y + down_boundary_y + right_boundary_y + up_boundary_y
    bound_vec = []
    for i in range(0, len(bound_x)):
        bound_vec.append(bound_x[i])
        bound_vec.append(bound_y[i])
    

    # 用这个
    if scenario == "parallel":
        # obstacles for distance approach(vertices coords in clock wise order)障碍物
        ROI_distance_approach_parking_boundary = (
            c_double * 20)(*bound_vec)
        OpenSpacePlanner.AddObstacle(
            ROI_distance_approach_parking_boundary)
        # parking lot position起始位置
        ex = end_x
        ey = -1.4+offset
        ephi = 0
        # 路径限制，路径边界
        XYbounds = [min(bound_x), max(bound_x), min(bound_y), max(bound_y)]

    x = (c_double * num_output_buffer)()
    y = (c_double * num_output_buffer)()
    phi = (c_double * num_output_buffer)()
    v = (c_double * num_output_buffer)()
    a = (c_double * num_output_buffer)()
    steer = (c_double * num_output_buffer)()
    opt_x = (c_double * num_output_buffer)()
    opt_y = (c_double * num_output_buffer)()
    opt_phi = (c_double * num_output_buffer)()
    opt_v = (c_double * num_output_buffer)()
    opt_a = (c_double * num_output_buffer)()
    opt_steer = (c_double * num_output_buffer)()
    opt_time = (c_double * num_output_buffer)()
    opt_dual_l = (c_double * num_output_buffer)()
    opt_dual_n = (c_double * num_output_buffer)()
    size = (c_ushort * 1)()
    XYbounds_ctype = (c_double * 4)(*XYbounds)
    hybrid_time = (c_double * 1)(0.0)
    dual_time = (c_double * 1)(0.0)
    ipopt_time = (c_double * 1)(0.0)

    success = True
    start = time.time()

    if OpenSpacePlanner.DistancePlan(sx, sy, sphi, ex, ey, ephi, XYbounds_ctype):
        planning_time = time.time() - start
        print("\tWhen ending point x-coordinate is",format(ex, '.1f'),"y-coordinate is",format(ey,'.1f'),", the planning time is",format(planning_time,'.3f'),", planning success")
    if not OpenSpacePlanner.DistancePlan(sx, sy, sphi, ex, ey, ephi, XYbounds_ctype):
        planning_time = time.time() - start
        print("\tWhen ending point x-coordinate is",format(ex, '.1f'),"y-coordinate is",format(ey,'.1f'),", the planning time is",format(planning_time,'.3f'),", planning failed")
        success = False
    #   exit()
    
    '''print("planning time is " + str(planning_time))'''

    x_out = []
    y_out = []
    phi_out = []
    v_out = []
    a_out = []
    steer_out = []
    opt_x_out = []
    opt_y_out = []
    opt_phi_out = []
    opt_v_out = []
    opt_a_out = []
    opt_steer_out = []
    opt_time_out = []
    opt_dual_l_out = []
    opt_dual_n_out = []

    if visualize_flag:
        # load result
        OpenSpacePlanner.DistanceGetResult(x, y, phi, v, a, steer, opt_x,
                                           opt_y, opt_phi, opt_v, opt_a, opt_steer, opt_time,
                                           opt_dual_l, opt_dual_n, size,
                                           hybrid_time, dual_time, ipopt_time)

        
        
        return success


if __name__ == '__main__':
    visualize_flag = True
    sx = 0.0
    sy = 3.0
    sphi = 0.0
    offset=0.0
    end_x_list=[1.3, 2.2, 2.6, 3.0, 3.5, 3.9, 4.1, 4.5, 4.9, 5.2]
    list1=list(range(0))
    print("Begin the source test:")
    print("Source input:")
    print("\tOffset of whole image is ",offset)
    print("\tStart point is (", sx,",", sy,")")
    print("\tInitialize 10 different ending points", end_x_list)
    for end_x in end_x_list:
        list1.append(SmoothTrajectory(visualize_flag, sx, sy, sphi, end_x, offset))
    print("Source output:")
    print("\tCorresponding output set is:", list1)
    
    #move whole image
    offset=[-3.0, 2.5, 4.7]
    for a in offset:
        sy=3.0+a
        list2=list(range(0))
        print("-------------------------------------------------------------------------------------------------------------------")
        print("Begin the follow-up test:")
        print("Follow-up input:")
        print("\tOffset of whole image is ",a)
        print("\tStart point is (", sx,",", sy,")")
        end_x_list2=list(range(10))
        for end_x in end_x_list:
            list2.append(SmoothTrajectory(visualize_flag, sx, sy, sphi, end_x, a))
        print("Follow-up output:")
        print("\tCorresponding output set is:", list2)

        #whether testing pass
        success=True
        list_difference=list(range(0))
        for i in range(10):
            if list1[i]!=list2[i]:
                success=False
                list_difference.append(end_x_list[i])
        if success==False:
            print("When input of ending point is in set" ,list_difference, ", then output relation is not satisfied.")
            print("Metamorphic testing failed!")
        else:
            print("For every input of ending point, output relation is satisfied.")
            print("Metamorphic testing success!")
    sys.exit()
